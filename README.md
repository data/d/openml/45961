# OpenML dataset: (Non-)depressive_tweet_data

https://www.openml.org/d/45961

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Description:
The dataset, named "clean_tweet_Dec19ToDec20.csv," comprises a collection of tweets post-processed for clarity and analysis, spanning from December 2019 to December 2020. It is designed to provide insights into public sentiment during this period, capturing a unique blend of personal and societal narratives emerging from various global circumstances, including the COVID-19 pandemic. This dataset is structured into columns that include an index for unique identification, the raw text of each tweet, and a sentiment score.

Attribute Description:
- Index: A numerical identifier assigned to each tweet, e.g., 98655, 59794.
- Text: Contains the cleaned and processed text of the tweet. This column captures a wide range of topics, from personal appliance purchases and mental health advice to discussions on electricity waste, unemployment, and even cryptocurrency-related dietary suggestions.
- Sentiment: A numerical sentiment score assigned to each tweet, where 0 indicates a negative sentiment and 1 indicates a positive sentiment. This binary classification assists in sentiment analysis, offering a simplistic yet effective insight into the general mood of each tweet.

Use Case:
This dataset can be instrumental for researchers and data scientists focusing on natural language processing (NLP), sentiment analysis, and trend spotting. It offers a rich resource for training machine learning models aimed at understanding public sentiment, detecting shifts in societal concerns or interests over time, and exploring the correlation between external events and public mood. Additionally, marketing professionals might leverage this dataset to gauge consumer sentiment, optimize brand communication strategies, and identify potential areas for product or service improvements based on public feedback.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45961) of an [OpenML dataset](https://www.openml.org/d/45961). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45961/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45961/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45961/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

